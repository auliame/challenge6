package id.auliamr.binar.myapplication.Database

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Entity
@Parcelize
data class User(

    @PrimaryKey
    var email: String,

    @ColumnInfo(name = "nama")
    var nama: String,

    @ColumnInfo(name = "password")
    var password: String,

    @ColumnInfo(name = "nama_lengkap")
    var fullName : String,

    @ColumnInfo(name = "alamat")
    var address : String,

    @ColumnInfo(name = "tgl_lahir")
    var dateOfBirth : String,

    ) : Parcelable