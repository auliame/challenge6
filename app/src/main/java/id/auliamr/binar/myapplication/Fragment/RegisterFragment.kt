package id.auliamr.binar.myapplication.Fragment

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import id.auliamr.binar.myapplication.Database.AppDatabase
import id.auliamr.binar.myapplication.Database.User
import id.auliamr.binar.myapplication.R
import id.auliamr.binar.myapplication.databinding.FragmentRegisterBinding
import kotlinx.android.synthetic.main.fragment_register.*
import kotlinx.coroutines.*
import java.util.regex.Pattern

class RegisterFragment : Fragment() , View.OnClickListener {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private var appDatabase : AppDatabase? = null

    private lateinit var name: String
    private lateinit var email: String
    private lateinit var password: String
    private var cek: Boolean = false
    private var viewPass : Boolean = false
    private var viewKonfPass : Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appDatabase = AppDatabase.getInstance(requireContext())
        binding.apply {
            btnRegister.setOnClickListener(this@RegisterFragment)
            btnIcViewPass.setOnClickListener(this@RegisterFragment)
            btnIcViewKonfPass.setOnClickListener(this@RegisterFragment)
        }
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.btn_register -> {
                register()
            }
            R.id.btn_ic_view_pass ->{
                if (viewPass == false){
                    binding.apply {
                        btnIcViewPass.setImageResource(R.drawable.ic_stroke_eye)
                        inputBuatPass.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    }
                    viewPass = true
                }else{
                    binding.apply {
                        btnIcViewPass.setImageResource(R.drawable.ic_remove_eye)
                        inputBuatPass.transformationMethod = PasswordTransformationMethod.getInstance()
                    }
                    viewPass = false
                }
            }
            R.id.btn_ic_view_konfPass -> {
                if (viewKonfPass == false){
                    binding.apply {
                        btnIcViewKonfPass.setImageResource(R.drawable.ic_stroke_eye)
                        inputKonfPass.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    }
                    viewKonfPass = true
                }else{
                    binding.apply {
                        btnIcViewKonfPass.setImageResource(R.drawable.ic_remove_eye)
                        inputKonfPass.transformationMethod = PasswordTransformationMethod.getInstance()
                    }
                    viewKonfPass = false
                }
            }
        }
    }

    private fun register(){
        binding.apply {
            name = input_uname.text.toString()
            email = input_email.text.toString()
            password = input_buatPass.text.toString()
            cek = isValidEmail(email)
        }

        if (inputCheck(name, email, password, cek)){
            registerUser(name, email, password)
        }
    }

    private fun registerUser(name: String, email: String, password: String) {
        val user = User(email, name, password, "", "", "")
        GlobalScope.async {
            val cekUser = appDatabase?.UserDao()?.getUserRegistered(email)
            if (cekUser != null) {
                requireActivity().runOnUiThread {
                    Toast.makeText(
                        requireContext(),
                        "User dengan email ${user.email} sudah terdaftar",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                val result = appDatabase?.UserDao()?.registerUser(user)
                requireActivity().runOnUiThread {
                    if (result != 0.toLong()) {
                        Toast.makeText(
                            requireContext(),
                            "Sukses mendaftarkan ${user.email}, silakan mencoba untuk login",
                            Toast.LENGTH_LONG
                        ).show()
                        Navigation.findNavController(requireView())
                            .navigate(R.id.action_registerFragment_to_loginFragment)
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Gagal mendaftarkan ${user.email}, silakan coba lagi",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }

        }
    }


    private fun inputCheck(name: String, email: String, password: String, cek: Boolean) : Boolean {
        if (name.isEmpty() || email.isEmpty() || password.isEmpty() || !cek
            || binding.inputKonfPass.text.toString() != password || password.length < 6
        ) {
            if (name.isEmpty()) {
                binding.apply {
                    input_uname.setError("Username Tidak Boleh Kosong")
                    input_uname.requestFocus()
                }

            }
            if (email.isEmpty()) {
                binding.apply {
                    input_email.setError("Email Tidak Boleh Kosong")
                    input_email.requestFocus()
                }
            }
            if (password.isEmpty()) {
                binding.apply {
                    input_buatPass.setError("Password Tidak Boleh Kosong")
                    input_buatPass.requestFocus()
                }
            }
            if (!cek) {
                binding.apply {
                    input_email.setError("Email Tidak Sesuai Format")
                    input_email.requestFocus()
                }
            }
            if (binding.inputKonfPass.text.toString() != password) {
                binding.apply {
                    input_konfPass.setError("Password Tidak Sama")
                    input_konfPass.requestFocus()
                }

            }
            if (password.length < 6) {
                binding.apply {
                    input_buatPass.setError("Password minimal 6 karakter")
                    input_buatPass.requestFocus()
                }
            }
            return false
        }else{
            return true
        }
    }

    private fun isValidEmail(email: String): Boolean {
        val emailPattern = Pattern.compile(
            "[a-zA-Z0-9+._%\\-]{1,256}" +
                    "@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        )
        return emailPattern.matcher(email).matches()
    }

}