package id.auliamr.binar.myapplication.Api

import id.auliamr.binar.myapplication.Database.GetDetailMovieResponse
import id.auliamr.binar.myapplication.Database.GetPopularMovieResponse
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @GET("/3/movie/popular")
    fun getPopularMovie(
        @Query("api_key") api_key: String
    ): Call<GetPopularMovieResponse>

    @GET("/3/movie/{movie_id}")
    fun getDetailMovie(
        @Path("movie_id") movieId: Int,
        @Query("api_key") api_key: String
    ): Call<GetDetailMovieResponse>

}