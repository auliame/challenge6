package id.auliamr.binar.myapplication.Database

import com.google.gson.annotations.SerializedName
import id.auliamr.binar.myapplication.Database.Genre

data class GetDetailMovieResponse(
    @SerializedName("genres")
    val genres: List<Genre>,
    @SerializedName("original_title")
    val originalTitle: String,
    @SerializedName("overview")
    val overview: String,
    @SerializedName("release_date")
    val releaseDate: String,
    @SerializedName("poster_path")
    val posterPath: String
)